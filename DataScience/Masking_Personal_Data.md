# Masking Personal Data
 
The following techniques can be used for protecting personal data, and all come with certain risks, and varying difficulty levels. What you use depends on many contextual factors, including ethical and legal concerns around privacy we've discussed in the previous videos.

Substitution Where you trade a value for an existing value (for example, let's say you use the following fake credit card number 1234-5678-9012-3456 instead of the actual value) It is effective, but it carries the risk that substituted values may actually mean something as well, or that the original reason and function for storing the value gets lost.

Shuffling Where you scramble the values inside a colummn of data, thereby removing the association with the rows (for example, you take the following value pairs (Sarah, 1,420), (Bill, 493), (Sue, 661), and shuffle the values into the following: (Sarah, 493), (Bill, 661), (Sue, 1,420)). The danger is that they are still real values, and could potentially be reassociated.

Number and date variance Where you shift numbers or dates up or down/forward or back. (for example, 10-31-2017 becomes 11-31-2017 (moved forward). The value is then still a date, albeit no longer “accurate.”

Encryption Where you “encode” your data using an algorithm–pig latin is a light version of encryption, where you transfer the first consonant of each word to the end of the word (wikipedia gives the example of chicken soup: "ickenchay oupsay"). When you know the rule (or key), you can decipher the code. Modern encryption uses a variety of algorithms to do this–for example, your bank transfers are encrypted so you can digitally transfer money safely.

Nulling out or deletion You can simply delete the value from the record–however, you then lose data integrity when you do this.

Differential Privacy You can use statistical methods to incorporate noise into all the records in a data set. To learn more, check out the “Further Reading Section” next up.

“Big Data: A Tool for Inclusion or Exclusion? Understanding the Issues” (FTC Report January 2016) U.S. Federal Trade Commission. https://www.ftc.gov/system/files/documents/reports/big-data-tool-inclusion-or-exclusion-understanding-issues/160106big-data-rpt.pdf

“Big Data: A Report on Algorithmic Systems, Opportunity, and Civil Rights” (Executive Office of the President May 2016) U.S. Executive Office. https://obamawhitehouse.archives.gov/sites/default/files/microsites/ostp/2016_0504_data_discrimination.pdf

More on encryption: https://www.howtogeek.com/howto/33949/htg-explains-what-is-encryption-and-how-does-it-work/

More on differential privacy: https://privacytools.seas.harvard.edu/differential-privacy