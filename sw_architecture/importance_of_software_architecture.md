If you are reading this book today, you should thank the computer scientists who decided to consider software development as an engineering area. This happened in the last century and, more specifically, at the end of the sixties, when they proposed that the way we develop software is quite similar to the way we construct buildings. That is why we have the name software architecture. Like in the design of a building, the main goal of a software architect is to ensure that the software application is implemented well. But a good implementation requires the design of a great solution. Hence, in a professional development project, you have to do the following things:

    Define the customer requirements for the solution.
    Design a great solution to meet those requirements.
    Implement the designed solution.
    Validate the solution with your customer.
    Deliver the solution in the working environment.

Software engineering defines these activities as the software development life cycle. All of the theoretical software development process models (waterfall, spiral, incremental, agile, and so on) are somehow related to this cycle. No matter which model you use, if you do not work with the essential tasks presented earlier during your project, you will not deliver acceptable software as a solution.

The main point about designing great solutions is totally connected to the purpose of this book. You have to understand that great real-world solutions bring with them a few fundamental constraints:

    The solution needs to meet user requirements.
    The solution needs to be delivered on time.
    The solution needs to adhere to the project budget.
    The solution needs to deliver good quality.
    The solution needs to guarantee a safe and efficacious future evolution.

Great solutions need to be sustainable and you have to understand that there is no sustainable software without great software architecture. Nowadays, great software architectures depend on both tools and environments to perfectly fit users' requirements.